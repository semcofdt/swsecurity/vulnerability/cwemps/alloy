/**
* Vulnerability Module
*
* Define representative vulnerabilities for MPS communication in a Component Base Architecture
* using Message Passing Asynchronous based communication model
*/
module vulnerability

open Architecture/cbsemetamodel
open Architecture/connectorMPS
open Architecture/constraint
open Architecture/property
open util/ordering[cbsemetamodel/Tick] as tick

// Information exposure 
sig PayloadExposure extends ConnectorProperty {}{
	not( all  t:Tick  | all d:Payload | E_get_pld[c2,m,d,t] => sent_to[m,c2,t])
}

assert PayloadExposureExits {
	 all con:Connector, m:MsgExch | no v:PayloadExposure| v.holds[con,m]
}

check PayloadExposureExits for 5

pred Connector.OnlySenderAndReceiverCanConnect {
	all m:MsgExch | this = m.connector implies this.connects = m.sender.ports + m.receiver.ports
}

assert OnlySenderAndReceiverCanConnectMitigatePayloadExposure{
    all con:Connector |  con.OnlySenderAndReceiverCanConnect => all m:MsgExch |  no v:PayloadExposure| v.holds[con,m]
}
check OnlySenderAndReceiverCanConnectMitigatePayloadExposure for 5

// Improper authentication 
sig SenderSpoofing extends ConnectorProperty {}{
	not (all t:Tick | E_get_src[c2,m,c1,t] => sent_by[m,c1,t] )
}

assert SenderSpoofingExits {
	  all c:Connector, m:MsgExch | no v:SenderSpoofing | v.holds[c,m]
}

check SenderSpoofingExits for 5

pred Connector.SenderIsAuthentic {
	all c:Component, m:MsgExch, t:Tick | inject[c,this,m,t] => m.sender = c
}

assert SenderIsAuthenticMitigateSenderSpoofing {
    all c:Connector | c.SenderIsAuthentic => all  m:MsgExch | no v:SenderSpoofing | v.holds[c,m]
}

check SenderIsAuthenticMitigateSenderSpoofing for 5

// Incorrect permission assignment for critical resource 
sig BadPrivilegeVerification extends ComponentProperty {}{
	not( all con:Connector, t,t':Tick  |
		(inject[c,con,m,t] => Z_inject[c,m,t])
		and
		(intercept[c,con,m,t'] => Z_intercept[c,m,t'])
	)
}

assert BadPrivilegeVerificationExits {
	 all c:Component, m:MsgExch | no v:BadPrivilegeVerification | v.holds[c,m]
}

check BadPrivilegeVerificationExits for 5

pred Component.InjectInterceptRequireAuth {
	all con:Connector, m:MsgExch, t:Tick  |
		(E_inject[this,con,m,t] => Z_inject[this,m,t])
		and
		(E_intercept[this,con,m,t] => Z_intercept[this,m,t])
}

assert InjectInterceptRequireAuthMitigateBadPrivilegeVerification {
    all c:Component | c.InjectInterceptRequireAuth=> all  m:MsgExch | no v:BadPrivilegeVerification | v.holds[c,m]
}
check InjectInterceptRequireAuthMitigateBadPrivilegeVerification for 5


// Improper privilege management 
sig BadPrivilegeManagement extends ComponentProperty {}{
	not (all t:Tick |
		Z_inject[c,m,t] => some c2:Component - c | Z_inject[c, c2,m,t] 
		and 
		Z_intercept[c,m,t] =>  some c2:Component - c | Z_intercept[c, c2,m,t] 
	)
}
 
assert BadPrivilegeManagementExits {
	 all c:Component, m:MsgExch | no v:BadPrivilegeManagement | v.holds[c,m]
}

check BadPrivilegeManagementExits for 5

pred Component.ComponentPrivilegeAssignementAreLogged {
	all ac: Authorization | ac.clnt = this =>  some c:Component-this | c = ac.addBy
}

assert PrivilegeAssignementAreLoggedMitigateBadPrivilegeManagement {
    all c:Component | c.ComponentPrivilegeAssignementAreLogged => all  m:MsgExch | no v:BadPrivilegeManagement | v.holds[c,m]
}
check PrivilegeAssignementAreLoggedMitigateBadPrivilegeManagement for 5


//Uncontrolled release consumption 
sig ConnectorBufferLimitedRate extends ConnectorProperty {}{
	not( all t:Tick  | inject[c2,con,m,t]  => has_slot[con,c2,t])
}

assert ConnectorBufferLimitedRateExits {
	 all con:Connector, m:MsgExch | no v:ConnectorBufferLimitedRate | v.holds[con,m]
}

check ConnectorBufferLimitedRateExits for 5

pred Connector.DropInjectWhenNoSlot {
	all t:Tick, c:Component, m:MsgExch | E_inject[c,this,m,t] => # buffered[this].t < this.size and (let m' = { m':buffered[this].t | m'.sentBy = c } | # m' < this.rate)
}

assert DropInjectWhenNoSlotMitigateConnectorBufferLimitedRate {
    all con:Connector |  con.DropInjectWhenNoSlot => all m:MsgExch | no v:ConnectorBufferLimitedRate | v.holds[con,m]
}

check DropInjectWhenNoSlotMitigateConnectorBufferLimitedRate for 5


// Missing release of resource after effective lifetime 
sig UnreleasedBuffer extends ConnectorProperty {}{
	not (all  t:Tick  | intercept[c1,con,m,t]  => not  buffered_by[con,m,t]) 
}

assert UnreleasedBufferExits {
	 all con:Connector, m:MsgExch | no v:UnreleasedBuffer | v.holds[con,m]
}

check UnreleasedBufferExits for 5

pred Connector.NotKeepingMsg {
	all c:Component, m:MsgExch, t:Tick  | intercept[c,this,m,t]  => m not in buffered[this].t
}

assert NotKeepingMsgMitigateUnreleasedBuffer  {
    all con:Connector |  con.NotKeepingMsg => all m:MsgExch | no v:UnreleasedBuffer | v.holds[con,m]
}

check NotKeepingMsgMitigateUnreleasedBuffer for 5

