/**
* MPS Module
*
* Message Passing Asynchronous based communication model for Component Base Architecture
*
*/
module mps

open cbsemetamodel

sig CAsync in Connector {}{}

sig CChannel in Connector {}{
	lone this.inputs
	lone this.outputs
}

sig CMPS in Connector {}{
	all i:Interaction |	this = i.connector => i in MsgExch // CMPS is an  MsgExch Interactioon
	all p:Port | p in connects => p in DataPort // MsgExch use data port
}

sig CSizeLimited in Connector {
	size : one Int,
	rate: one Int
}{}

abstract sig CChannelMPSAsync extends Connector {}
fact {
	CChannelMPSAsync in CAsync
	CChannelMPSAsync in CChannel
	CChannelMPSAsync in CMPS
	CChannelMPSAsync in CSizeLimited
}

abstract sig MsgExch extends Interaction {
	payload: set Payload,
	sentBy: one Component,
	sentAt: one Tick
}{
	# sender = 1
	# receiver = 1
	all c:Component, t:Tick | (c in sent.t => (c = sentBy and t = sentAt))
	all c:Component, t:Tick | (c in received.t implies one c2:Component, t2:t.prevs+t | c2 in sent.t2) // Received was sent
	all c:Component, t:Tick | (c in received.t implies all t2:t.nexts | c not in received.t2) // AtmostOneRecvInstant
	connector in CMPS
}

abstract sig Payload {}

fun buffered[con:Connector]: Interaction -> Tick{{i: Interaction, t: Tick | t.gte[i.sentAt] and some c:Component, t2:t.nexts + t | c in i.received.t2 and i.connector = con }} 

pred buffered_by[con:Connector,i:Interaction,t:Tick]{
	i in buffered[con].t  
}

pred has_slot[con:Connector, c:Component, t:Tick] {
	# buffered[con].t <= con.size
	let  m = { m:buffered[con].t | m.sentBy = c } | # m <  con.rate
}

/***
	Communication primitives
******************************************************************************/

/* */
// A Component (this) send a message d to a Component c at a Tick t
pred Component.send[c:Component,m:MsgExch,t:Tick] {
		m.receiver = c
		this = m.sentBy
		t = m.sentAt
}

// A Component (this) receive a message d from a Component c at a Tick t
pred Component.receive[c:Component,m:MsgExch, t:Tick] {
		m.sender = c
		this = m.received.t
}
