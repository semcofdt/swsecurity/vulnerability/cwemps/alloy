/**
* CBSE Module
*
* Architecture metamodel for Component Base Architecture
*/
module cbse
open util/ordering[Tick] as Tick
sig Tick {}

/**
	Component
*/
sig Component {}

fun Component.ports[]: set Port {
	ownedBy.this
}

fun Component.inputs[]: set Port {
	{ p:Port | p in ownedBy.this and p.kind = INPUT }
}

fun Component.outputs[]: set Port {
	{ p:Port | p in ownedBy.this and p.kind = OUTPUT }
}

/**
	Port
*/
abstract sig Port {
	ownedBy: one Component,
	kind: one PortKind
}
enum PortKind { INPUT, OUTPUT}
abstract sig DataPort extends Port {}

/**
	Connector
*/
abstract sig Connector {
	connects: set Port
}

fun Connector.inputs[]: set Port {
	{ p:Port | p in this.connects and p.kind = OUTPUT }
}

fun Connector.outputs[]: set Port {
	{ p:Port | p in this.connects and p.kind = INPUT }
}

/**
	Interaction
*/
abstract sig Interaction {
	connector:  one Connector,
	sender: set Component,
	receiver: set Component,
	received: set Component -> Tick,
	sent: set Component -> Tick
}{
	all c:Component, t:Tick  | (c in sent.t implies some p:Port | p in connector.inputs and p in c.outputs)  //Sending component have a corresoponding output port
	all c:Component, t:Tick | (c in received.t implies some p:Port | p in connector.outputs and p in c.inputs) //Receiving components have a corresponding input port
}
